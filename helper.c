
#include <stdio.h>
#include <sys/mman.h>
#include <allocator.h>

int main()
{
    size_t max_size = GB_SZ*256;

    void *tmp = mmap(NULL, max_size, PROT_NONE, MAP_PRIVATE | MAP_ANON | MAP_NORESERVE, -1, 0);
    while (tmp == MAP_FAILED) {
        max_size -= GB_SZ;
        tmp = mmap(NULL, max_size, PROT_NONE, MAP_PRIVATE | MAP_ANON | MAP_NORESERVE, -1, 0);
    }
    printf("Highest map size available is %llu GB_SZ\n", max_size/GB_SZ);
    return 0;
}
