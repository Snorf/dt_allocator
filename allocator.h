#ifndef __DT_ALLOCATOR_H
#define __DT_ALLOCATOR_H

#include <auxiliary.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <errno.h>


#define ALLOC_DEBUGS 0
#if ALLOC_DEBUGS
#define debug(...) \
    printf(__VA_ARGS__); \
    printf("\n");
#else
#define debug(...)
#endif

/* Memory searching algorithms. This will be used when there is free memory in between
 * allocated memory (caused by free's).
 * SEARCH_NONE:      the memory will always be allocated at the end of the stack. 
 *                   Faster, but increases fragmentation
 * SEARCH_BEST_FIT:  the memory will be placed in the best fit place within the arena.
 *                   The best fit is decided by the get_best_node function. The default
 *                   one is to find the smallest possible place that allows for the 
 *                   allocation.
 *                   Slower, but prevents fragmentation.
 * SEARCH_FIRST_FIT: the memory will be allocated in the first memory block that has
 *                   enough size to fit it.
 *                   Faster than SEARCH_BEST_FIT, but may have worse performance when
 *                   it comes to memory.
 * */
#define SEARCH_NONE      0
#define SEARCH_BEST_FIT  1
#define SEARCH_FIRST_FIT 2
#define SEARCH_STRATEGY SEARCH_BEST_FIT

/* When set to 1, once the allocator runs out of virtual memory addresses it will
 * attempt to grow the valid addresses. It should not be necessary with the default
 * arena sizes. */
#define GROW_ARENA 0


/* When set to 1, there will be a lock to ensure that concurrent calls to dt_alloc
 * do not cause issues. Set to 0 only if allocations for each arena only happen on
 * one thread. */
#define THREAD_SAFE 1

#define GB_SZ 1000000000ULL
#define MB_SZ 1000000ULL
/* Defines how big the initial memory mapping will be. If the mapping fails, it will try to do
 * a mapping of half the size until it works. Use the helper program to determine the best value */
#define MAX_ARENA (256*GB_SZ)

/* ARENA_PAGES defines the number of pages allocated per arena memory increase. If there are 
 * large allocations, it may be worth to increase this value to slightly speed them up*/
#define ARENA_PAGES 8
/* Set ADVISES to 1 to use the madvise function that gives hints of memory utilization to 
 * the OS. It may be useful with a large number of ARENA_PAGES (say, commiting 1GB of memory
 * at a time when it may not be needed), but otherwise leave it at 0 */
#define ADVISES 0
/* Number of BYTES that the memory needs to be aligned to. 8 -> 64 bits, which is usually what
 * memory is aligned to on x64 systems. Change to suit the target architecture. */
#define PADD_ALIGNMENT 8

/* RESET_TO_ZERO defines whether ALL memory is guaranteed to be set to 0. If memory is never
 * free'd or realloc'd, this does not matter (since our mmap usage gives zeroed memory), but
 * if we use free or realloc, we need to know whether to set the "old" memory back to zero or
 * not. 
 * If set to 1, new memory allocations are always set to 0.
 * If set to 0, new memory allocations MIGHT not be 0. */
#define RESET_TO_ZERO 0

/* Sets the default allocator function dt_alloc. Currently the two options are
 * dt_alloc_basic
 * dt_alloc_growable
 *
 * The individual functions can still be called, this is for ease of use.
 */
#define DEFAULT_ALLOC dt_alloc_basic
#define dt_alloc(...) DEFAULT_ALLOC(__VA_ARGS__)

/* Base arena structure */
typedef struct arena {
    char  *pool;
    size_t offset;
    size_t num_blocks;
#if THREAD_SAFE
    pthread_mutex_t lock;
#endif
    search_struct_t first_free;
    size_t size; //to actually have the same as any "node"
} arena_t;

typedef struct arena_scratch {
    arena_t *parent;
    size_t original_offset;
} arena_scratch_t;

/* Small structure that is created when
 * allocating memory and used when resizing buffers */
typedef struct node {
    search_struct_t next_free;
    size_t size; //size comes after so it is more intuitive to access from the search_struct
} node_t;

#define dt_alloc_basic(type, size)  _Generic((type), \
                                    arena_scratch_t: dt_alloc_scratch_basic, \
                                    default: dt_alloc_arena_basic)(type, size)

#define dt_alloc_growable(type, size) _Generic((type), \
                                      arena_scratch_t: dt_alloc_scratch_growable, \
                                      default: dt_alloc_arena_growable)(type, size)

/** ARENA functions */
/* Creates the arena and prepares the memory address pool */
arena_t dt_arena_create();
/* Allocates ssize bytes in the arena */
void *dt_alloc_arena_basic(arena_t *arena, size_t ssize);
/* Allocates M*ssize bytes in the arena, where M is chosen by the ssize and the GROW_MULTIPLIER*/
void *dt_alloc_arena_growable(arena_t *arena, size_t ssize);
/* Reallocates ptr into a fitting size of the arena */
void *dt_realloc(arena_t *arena, void *ptr, size_t new_size);
/* Frees the memory pointed by ptr */
void  dt_free(arena_t *arena, void *ptr);
/* Frees all the memory in the arena and resets the structure */
void  dt_arena_destroy(arena_t *arena);


/** SCRATCH functions */
/* Creates a scratch arena from the current arena */
arena_scratch_t dt_scratch_create(arena_t *arena);
/* Allocates ssize bytes in the scratch arena*/
void *dt_alloc_scratch_basic(arena_scratch_t *scratch, size_t ssize);
/* Allocates M*ssize bytes in the scratch arena, where M is chosen by the ssize and the GROW_MULTIPLIER*/
void *dt_alloc_scratch_growable(arena_scratch_t *scratch, size_t ssize);

/* Destroys the scratch arena and resets the stack pointer to where it was before its creation */
void dt_scratch_destroy(arena_scratch_t *scratch);



#endif
