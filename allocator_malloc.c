
#include <allocator.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>

#include <errno.h>

#define MAX_ARENA 8000000000//64 GBytes
#define ARENA_PAGES 16
#define PADD_ALIGNMENT 8

static int page_size = 0;
static size_t max_size = 0;

/* 
 * Creates the arena with a potential size of MAX_ARENA.
 * mmap with PROT_NONE does not actually allocate any memory, but reservers the 
 * necessary CONTIGUOUS memory pages for when we need to allocate the data.
 */
arena_t dt_arena_create()
{
    if (!max_size) {
        max_size = MAX_ARENA*64;
    }

    arena_t a = { 0 };
    a.pool = malloc(max_size);

    while (a.pool == NULL) { //dirty, but it works
        max_size /= 2;
        a.pool = malloc(max_size);
    }
    debug("Pool set with virtual memory starting at %p", a.pool);
    a.num_blocks = 1;

    return a;
}


/*
 * Allocates memory of ssize+sizeof(node_t) size in the arena.
 *
 * First it changes the protection of the new memory region, which allows the memory to be read/write
 * and signals the OS that the allocation is in action. Then it advises that the memory allocated will
 * be used (since _most_ allocations are done immediately before their use).
 * First it copies the information of the allocation on the first free memory region in the stack,
 * then it allocates the memory requested and returns the pointer at the beginning of the region.
 * No need to set the memory to 0 since mmap does this automatically for MAP_ANON mappings.
 */
void *dt_alloc_basic(arena_t *arena, size_t ssize)
{
    size_t new_offset = arena->offset + ssize + sizeof(node_t);
    size_t padd_offset = new_offset % PADD_ALIGNMENT;
    if (padd_offset) {
        ssize += PADD_ALIGNMENT-padd_offset;
    }

    void *ptr = &arena->pool[arena->offset+sizeof(node_t)];

    node_t *node = (node_t *)&arena->pool[arena->offset];
    node->size = ssize;

    arena->offset = new_offset;
    return ptr;
}

size_t get_grow_size(size_t initial_size)
{
    size_t fsize = initial_size;
    for (int i = 0; i < NUM_GROW_THRESHOLDS; i++) {
        if (fsize < GROW_THRESHOLDS[i]) {
            return (fsize *= GROW_MULTIPLIER[i]);
        }
    }
    return fsize;
}

/* Like dt_alloc_basic but prepares a larger chunk of data in case we may need to realloc */
void *dt_alloc_growable(arena_t *arena, size_t ssize)
{
    size_t fsize = get_grow_size(ssize);
    size_t new_offset = arena->offset + fsize + sizeof(node_t);
    size_t padd_offset = new_offset % PADD_ALIGNMENT;
    if (padd_offset) {
        fsize += PADD_ALIGNMENT-padd_offset;
    }

    debug("DT alloc: allocating from space %p to %p (size %lu)", &arena->pool[arena->offset], &arena->pool[new_offset], new_offset-arena->offset);
    if (new_offset >= MAX_ARENA) {
        return NULL;
        debug("Insufficient memory, entering new block");
        void *tmp = memset(&arena->pool[ARENA_PAGES*page_size*arena->num_blocks], 0, ARENA_PAGES*page_size); 
        if (tmp == NULL) {
            debug("Mapping failed (errno %d, error %s)", errno, strerror(errno));
            return NULL;
        }
        arena->num_blocks++;
        debug("new end of block %p", &arena->pool[ARENA_PAGES*page_size*arena->num_blocks]);
    }

    node_t *node = (node_t *)&arena->pool[arena->offset];
    memset(node, 0, sizeof(node_t));
    node->size = fsize;

    void *ptr = memset(&arena->pool[arena->offset+sizeof(node_t)], 0, fsize);
    arena->offset = new_offset;
    return ptr;
}

void *dt_realloc(arena_t *arena, void *ptr, size_t new_size)
{
    node_t *c_node = (node_t*)(&((char *)ptr)[-sizeof(node_t)]);
    if (c_node->size >= new_size) return ptr; //no need to change anything

    /* Free the current node and simplify calculations later */
    c_node->freed = true;

    /* Use growable since we are realloc-ing once, we may call it again */
    void *new_ptr = dt_alloc_growable(arena, new_size);

    memcpy(new_ptr, ptr, c_node->size);
    return new_ptr;
}

void dt_free(arena_t *arena, void *ptr)
{
    node_t *tmp = (node_t *) (&((char *)ptr)[-sizeof(node_t)]);
    tmp->freed = true;
    // if it's the last one, we simply move back the stack pointer
    if (&arena->pool[arena->offset] == ptr + tmp->size) {
        arena->offset -= tmp->size + sizeof(node_t);
    }
}

void dt_arena_destroy(arena_t *arena)
{
    free(arena->pool);
    arena->offset = 0;
    arena->num_blocks = 0;
}
