
#include <stdint.h>
#include <allocator.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

static int page_size = 0;
static size_t max_size = 0;


#if THREAD_SAFE
#define dt_lock(arena)   pthread_mutex_lock(&arena->lock)
#define dt_unlock(arena) pthread_mutex_unlock(&arena->lock)
#else
#define dt_lock(arena)
#define dt_unlock(arena) 
#endif

/* 
 * Creates the arena with a potential size of MAX_ARENA.
 * mmap with PROT_NONE does not actually allocate any memory, but reservers the 
 * necessary CONTIGUOUS memory pages for when we need to allocate the data.
 * If ADVISES is defined, the memory region is set as probably not needed.
 */
arena_t dt_arena_create()
{
    if (!page_size) {
        page_size = getpagesize();
    }

    if (!max_size) {
        max_size = MAX_ARENA*64;
    }

    arena_t a = { 0 };
    a.pool = mmap(NULL, max_size, PROT_NONE, MAP_PRIVATE | MAP_ANON | MAP_NORESERVE, -1, 0);

    while (a.pool == MAP_FAILED) { //dirty, but it works
        max_size /= 2;
        debug("First mapping failed of size %lu (errno %d, error %s)", max_size, errno, strerror(errno));
        a.pool = mmap(NULL, max_size, PROT_NONE, MAP_PRIVATE | MAP_ANON | MAP_NORESERVE, -1, 0);
    }
    if (mprotect(a.pool, ARENA_PAGES*page_size, PROT_READ | PROT_WRITE)) {
        fprintf(stderr, "Error setting PROT's to memory (%s)\n", strerror(errno));
    }
    debug("Pool set with virtual memory starting at %p, ending at %p", a.pool, &a.pool[ARENA_PAGES*page_size]);
    a.num_blocks = 1;
#if ADVISES
    madvise(a.pool, ARENA_PAGES*page_size, MADV_DONTNEED);
#endif
#if THREAD_SAFE
    pthread_mutex_init(&a.lock, NULL);
#endif
    a.size = SIZE_MAX;

    return a;
}

/* Aligns ssize according to the number of bytes specified in PADD_ALIGNMENT.
 *
 * First it checks if any bytes to the right of the PADD_ALIGNMENT are set to 1. If they
 * are, we increase the offset to the closest PADD_ALIGNMENT multiplier. If not we do nothing.
 *
 * Why use the alignment on ssize?
 * 1- the offset starts at 0 which is guaranteed to be aligned.
 * 2- the size of node_t MUST be multiple of PADD_ALIGNMENT (TODO: make sure of this)
 * 3- if new_offset = previous_offset + ssize + sizeof(node_t), we only need to ensure that
 *          ssize is a multiple of PADD_ALIGNMENT to guarantee that the new offset will also
 *          be properly aligned.
 */
static inline size_t align_alloc(size_t ssize) {
    size_t must_align = ssize & (PADD_ALIGNMENT - 1);
    return must_align ? (ssize + PADD_ALIGNMENT - must_align) : ssize;
}

/*
 * Allocates memory of at least ssize+sizeof(node_t) size in the arena.
 *
 * First it changes the protection of the new memory region, which allows the memory to be read/write
 * and signals the OS that the allocation is in action. Then it advises that the memory allocated will
 * be used (since _most_ allocations are done immediately before their use).
 * First it copies the information of the allocation on the first free memory region in the stack,
 * then it allocates the memory requested and returns the pointer at the beginning of the region.
 * No need to set the memory to 0 since mmap does this automatically for MAP_ANON mappings.
 */
void *dt_alloc_arena_basic(arena_t *arena, size_t ssize)
{
    dt_lock(arena);
    ssize = align_alloc(ssize);

    void *ptr = NULL;
    ptr = find_free_node(&arena->first_free, ssize);
    if (ptr) {
        goto alloc_exit;
    }

    size_t new_offset = arena->offset + ssize + sizeof(node_t);

    debug("DT alloc: allocating from space %p to %p (size %lu)", &arena->pool[arena->offset], &arena->pool[new_offset], new_offset-arena->offset);
    while (new_offset >= ARENA_PAGES*page_size*arena->num_blocks) {
        debug("Insufficient memory, entering new block");
#if GROW_ARENA 
        if (new_offset > max_size) {
            void *tmp = mmap(&arena->pool[ARENA_PAGES*page_size*arena->num_blocks], max_size, PROT_NONE, MAP_FIXED | MAP_ANON | MAP_PRIVATE, -1, 0);
            if (tmp == MAP_FAILED) {
                frprintf(stderr, "Mapping failed (errno %d, error %s)\n", errno, strerror(errno));
                return NULL;
            }
        }
#endif
        if (mprotect(&arena->pool[ARENA_PAGES*page_size*arena->num_blocks], 
                      ARENA_PAGES*page_size, PROT_READ | PROT_WRITE)) {
            fprintf(stderr, "Error setting PROT's to memory (%s)\n", strerror(errno));
        }
        arena->num_blocks++;
        debug("new end of block %p", &arena->pool[ARENA_PAGES*page_size*arena->num_blocks]);
    }

    ptr = &arena->pool[arena->offset+sizeof(node_t)];

    node_t *node = (node_t *)&arena->pool[arena->offset];
    node->size = ssize;

    arena->offset = new_offset;

alloc_exit:
    dt_unlock(arena);
    return ptr;
}

/* Like dt_alloc_basic but prepares a larger chunk of data in case we may need to realloc */
void *dt_alloc_arena_growable(arena_t *arena, size_t ssize)
{
    dt_lock(arena);

    size_t fsize = get_grow_size(ssize);
    fsize = align_alloc(fsize);

    void *ptr = NULL;
    ptr = find_free_node(&arena->first_free, ssize);
    if (ptr) {
        goto alloc_exit;
    }
    size_t new_offset = arena->offset + fsize + sizeof(node_t);

    debug("DT alloc: allocating from space %p to %p (size %lu)", 
            &arena->pool[arena->offset], &arena->pool[new_offset], new_offset-arena->offset);

    while (new_offset >= ARENA_PAGES*page_size*arena->num_blocks) {
        debug("Insufficient memory, entering new block");
#if GROW_ARENA
        if (new_offset > max_size) {
            void *tmp = mmap(&arena->pool[ARENA_PAGES*page_size*arena->num_blocks], 
                             ARENA_PAGES*page_size, 
                             PROT_READ | PROT_WRITE, 
                             MAP_FIXED | MAP_ANON | MAP_PRIVATE, -1, 0);
            if (tmp == MAP_FAILED) {
                fprintf(stderr, "Mapping failed (errno %d, error %s)\n", errno, strerror(errno));
                return NULL;
            }
        }
#endif
        if (mprotect(&arena->pool[ARENA_PAGES*page_size*arena->num_blocks], 
                     ARENA_PAGES*page_size, PROT_READ | PROT_WRITE)) {
            fprintf(stderr, "Error setting PROT's to memory (%s)\n", strerror(errno));
        }
        arena->num_blocks++;
        debug("new end of block %p", &arena->pool[ARENA_PAGES*page_size*arena->num_blocks]);
    }
#if ADVISES
    madvise(&arena->pool[arena->offset],  new_offset-arena->offset, MADV_WILLNEED); 
#endif

    node_t *node = (node_t *)&arena->pool[arena->offset];
    node->size = fsize;

    ptr = &arena->pool[arena->offset+sizeof(node_t)];
    arena->offset = new_offset;

alloc_exit:
    dt_unlock(arena);
    return ptr;
}

//TODO: check whether it is the last pointer as a special case.
//      If it is, even if the alloc size is small we can simply
//      move the stack pointer again to increase the size without
//      any memmove
void *dt_realloc(arena_t *arena, void *ptr, size_t new_size)
{
    node_t *c_node = (node_t*)(&((char *)ptr)[-sizeof(node_t)]);
    if (c_node->size >= new_size) return ptr; //no need to change anything

    /* Use growable since we are realloc-ing once, we may call it again */
    void *new_ptr = dt_alloc_growable(arena, new_size);

    dt_lock(arena);
    free_node(&arena->first_free, &c_node->next_free);

    memcpy(new_ptr, ptr, c_node->size);
#if RESET_TO_ZERO
    memset(ptr, 0, c_node->size);
#endif
    dt_unlock(arena);
    return new_ptr;
}

void dt_free(arena_t *arena, void *ptr)
{
    dt_lock(arena);
    node_t *c_node = (node_t *) (&((char *)ptr)[-sizeof(node_t)]);
#if RESET_TO_ZERO
    memset(ptr, 0, c_node->size);
#endif
    // if it's the last one, we simply move back the stack pointer
    if (&arena->pool[arena->offset] == ptr + c_node->size) {
        arena->offset -= c_node->size + sizeof(node_t);
    }
    // if it is not, and we have a free structure, we look at it
    else {
        free_node(&arena->first_free, &c_node->next_free);
    }
    dt_unlock(arena);
}

void dt_arena_destroy(arena_t *arena)
{
    dt_lock(arena);
    munmap(arena->pool, ARENA_PAGES*page_size*arena->num_blocks);
    arena->offset = 0;
    arena->num_blocks = 0;
    dt_unlock(arena);
}

void *dt_alloc_scratch_basic(arena_scratch_t *scratch, size_t ssize)
{
    return dt_alloc_arena_basic(scratch->parent, ssize);
}

void *dt_alloc_scratch_growable(arena_scratch_t *scratch, size_t ssize)
{
    return dt_alloc_arena_growable(scratch->parent, ssize);
}

arena_scratch_t dt_scratch_create(arena_t *arena)
{
    return (arena_scratch_t) {arena, arena->offset};
}

void dt_scratch_destroy(arena_scratch_t *scratch)
{
    scratch->parent->offset = scratch->original_offset;
}


