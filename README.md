# Deixondit's allocator (dt_alloc)

Simple and configurable arena allocator for linux based on the mmap function.


TODO:
 - [ ] Create a small program to help determine ideal initial parameters
 - [X] Create (optional) free'd memory list
    - [X] Create tree traversal functions for memory allocation
    - [ ] Create another option in the form of a tree
 - [ ] Improve folder and export/install structure
 - [ ] Add single-header support
 - [ ] Windows support
