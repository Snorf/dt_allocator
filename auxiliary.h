#ifndef __AUXILIARY_H
#define __AUXILIARY_H

#include <stdlib.h>


/* Defines how dt_alloc_growable calculates the padded size of the allocation.
 * It goes through the list of GROW_THRESHOLDS, stops at the first occurrence of
 * size < GROW_THRESHOLDS[i] and actually allocates size*GROW_MULTIPLIER[i].
 * If no occurrence is found, if USE_LAST_MULTIPLIER is set to 1, it will use
 * the GROW_MULTIPLIER[NUM_GROW_THRESHOLDS-1] and return. Otherwise, the size will
 * not be changed.
 *
 * Set NUM_GROW_THRESHOLDS to 0 if you want dt_alloc_growable to behave like dt_alloc_basic
 */
#define NUM_GROW_THRESHOLDS 4
#define USE_LAST_MULTIPLIER 1
static const size_t GROW_THRESHOLDS[NUM_GROW_THRESHOLDS] = {10, 100, 1000, 10000};
static const size_t GROW_MULTIPLIER[NUM_GROW_THRESHOLDS] = {100, 10, 10, 10};


/* Defines the search structure for allocations on free'd memory regions.
 * If set to NONE, the allocator behaves purely as a stack allocator. */
#define NONE 0
#define DT_ALLOC_FREE_LIST 1
#define DT_ALLOC_RB_TREE   2
#define SEARCH_STRUCTURE DT_ALLOC_FREE_LIST
//#define SEARCH_STRUCTURE NONE 

#if SEARCH_STRUCTURE == DT_ALLOC_FREE_LIST
typedef struct free_node {
    struct free_node *next;
}free_node_t;
#define search_struct_t free_node_t
#define SEARCH_STRUCT_SIZE sizeof(search_struct_t)+PADD
#elif SEARCH_STRATEGY == DT_ALLOC_RB_TREE
typedef struct rb_node {
} rb_node_t;
#define search_struct_t rb_node_t
#define SEARCH_STRUCT_SIZE sizeof(search_struct_t)+PADD
#else 
#error SEARCH_STRUCTURE must be defined and set
#endif


/* Function that calculates the actual size to allocate by dt_alloc_growable.
 * Given an initial_size, returns the actual allocation size. */
size_t get_grow_size(size_t initial_size);

/* Given an initial node of a search structure (linked list/RB tree), inserts the
 * new node to_free in it for later use. */
void free_node(search_struct_t *initial, search_struct_t *to_free);

/* Given the initial node of a search structure, returns a pointer to the first memory
 * region that has enough free space to fit an allocation of ssize */
void *get_first_free_node(search_struct_t *initial, size_t ssize);
void *find_free_node(free_node_t *initial, size_t req_size);

#endif
