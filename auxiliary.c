
#include <auxiliary.h>
#include <allocator.h>

size_t get_grow_size(size_t initial_size)
{
    size_t fsize = initial_size;
    for (int i = 0; i < NUM_GROW_THRESHOLDS; i++) {
        if (fsize < GROW_THRESHOLDS[i]) {
            return (fsize *= GROW_MULTIPLIER[i]);
        }
    }
    return fsize;
}

#if SEARCH_STRUCTURE == DT_ALLOC_FREE_LIST
void free_node(free_node_t *initial, free_node_t *current_node)
{
    //the list is sorted from biggest chunk to smallest
    size_t size_to_fit = (size_t) current_node[1].next; //pointer fuckery
    free_node_t *curr = initial;
    while (curr->next){
        size_t curr_size = (size_t) curr[1].next; //pointer fuckery
        //if the new free'd value is bigger than our current node in the list,
        //we insert our node there and return
        //
        //NOTE: the first node has SIZE_MAX so this will always return false,
        //maybe there is a cleaner way to do this??
        if (size_to_fit >= curr_size) {
            current_node->next = curr->next;
            curr->next = current_node;
            return;
        }
        curr = curr->next;
    }

    //if we have not found the node, we add it to the last position
    curr->next = current_node;
    current_node->next = NULL;
}

void *find_free_node(free_node_t *initial, size_t req_size)
{
#if SEARCH_STRATEGY == SEARCH_FIRST_FIT
    //skip the initial one, since it's a placeholder anyway
    if (initial->next) {
        free_node_t *prev = initial;
        free_node_t *curr = initial->next;
        while (curr) {
            size_t curr_size = (size_t) curr[1].next; //pointer fuckery
            if (curr_size >= req_size) {
                prev->next = curr->next; //remove it from the list
                char *tmp = (char *) curr;
                return &tmp[sizeof(free_node_t)+sizeof(size_t)]; //size of node_t
            } else { 
                return NULL;
            }
            prev = curr;
            curr = curr->next;
        }
    }
    return NULL;
#elif SEARCH_STRATEGY == SEARCH_BEST_FIT
    //skip the initial one, since it's a placeholder anyway
    if (initial->next) {
        free_node_t *prev = initial;
        free_node_t *curr = initial->next;
        free_node_t *dprev = NULL;
        bool prev_potential = false;
        while (curr) {
            size_t curr_size = (size_t) curr[1].next; //pointer fuckery
            //if the current one can fit the required size (but it's not a perfect fit)
            //we store the previous of this and mark it as potential candidate
            if (curr_size > req_size) {
                prev_potential = true; 
                dprev = prev;
            } else if (curr_size == req_size) { //if we have a perfect fit we don't have to search anymore 
                prev->next = curr->next; //remove it from the list
                char *tmp = (char *) curr;
                return &tmp[sizeof(free_node_t)+sizeof(size_t)]; //size of node_t
            } else { 
                //if we had marked one as potentially big enough, we use that one. Otherwise, return NULL
                if (prev_potential) {
                    dprev->next = curr;
                    char *tmp = (char *) prev;
                    return &tmp[sizeof(free_node_t)+sizeof(size_t)]; //size of node_t
                }
                return NULL;
            }
            prev = curr;
            curr = curr->next;
        }
    }
    return NULL;
#else
    return NULL;
#endif
}
#elif SEARCH_STRUCTURE == DT_ALLOC_FREE_LIST
#else

#endif
