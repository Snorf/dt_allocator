
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <allocator.h>

#define NUM_ALLOCS 4096*96
#define ALLOC_SZ   4096

void custom_alloc_test()
{
    arena_t a = dt_arena_create();

    int32_t **tmp = dt_alloc(&a, sizeof(int32_t *)*NUM_ALLOCS);

    for (int32_t i = 0; i < NUM_ALLOCS; i++) {
        tmp[i] = dt_alloc(&a, sizeof(int32_t)*ALLOC_SZ);
        if (tmp[i] == NULL) break;
        memset(tmp[i], 1, sizeof(int32_t)*ALLOC_SZ);
        for (int32_t j = 0; j < ALLOC_SZ; j++) {
            tmp[i][j] = i+j;
        }
    }

    for (int32_t i = 0; i < NUM_ALLOCS; i++) {
        dt_free(&a, tmp[i]);
    }

    for (int32_t i = 0; i < NUM_ALLOCS; i++) {
        tmp[i] = dt_alloc(&a, sizeof(int32_t)*ALLOC_SZ);
    }

    dt_arena_destroy(&a);
}

void malloc_test()
{
    int32_t **tmp = calloc(NUM_ALLOCS, sizeof(int32_t *));

    for (int32_t i = 0; i < NUM_ALLOCS; i++) {
        tmp[i] = calloc(ALLOC_SZ, sizeof(int32_t));
        if (tmp[i] == NULL) break;
        memset(tmp[i], 1, sizeof(int32_t)*ALLOC_SZ);
        for (int32_t j = 0; j < ALLOC_SZ; j++) {
            tmp[i][j] = i+j;
        }
    }

    for (int32_t i = 0; i < NUM_ALLOCS; i++) {
        free(tmp[i]);
    }

    for (int32_t i = 0; i < NUM_ALLOCS; i++) {
        tmp[i] = calloc(sizeof(int32_t), ALLOC_SZ);
    }

    free(tmp);
}

int main(void)
{
    
    struct timespec t1 = { 0 };
    struct timespec t2 = { 0 };
    struct timespec t3 = { 0 };
    clock_gettime(CLOCK_REALTIME, &t1);
    custom_alloc_test();
    clock_gettime(CLOCK_REALTIME, &t2);
    malloc_test();
    clock_gettime(CLOCK_REALTIME, &t3);


    printf("Total custom alloc time %luns\n", t2.tv_nsec - t1.tv_nsec);
    printf("Total malloc time       %luns\n", t3.tv_nsec - t2.tv_nsec);
    printf("Ratio custom vs alloc %.2lf\n", (double)(t2.tv_nsec - t1.tv_nsec)/(t3.tv_nsec - t2.tv_nsec));

    if (t2.tv_sec - t1.tv_sec) {
        printf("Total custom alloc time %lus\n", t2.tv_sec - t1.tv_sec);
        printf("Total malloc time       %lus\n", t3.tv_sec - t2.tv_sec);
        if (t3.tv_nsec - t2.tv_sec) {
            printf("Ratio custom vs alloc %.2lf\n", (double)(t2.tv_sec - t1.tv_sec)/(t3.tv_sec - t2.tv_sec));
        }
    }
    return 0;
}
