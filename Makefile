

CC = gcc
CFLAGS = -O0 -Wall -Wextra -g -I.

ALLOC_OBJS = \
			 auxiliary.o \
			 allocator.o \


TEST_OBJS = \
			test

UTILS_OBJS = \
			 helper
			 
all: $(ALLOC_OBJS) $(UTILS_OBJS) $(TEST_OBJS) 

helper: 
	$(CC) $(CFLAGS) helper.c -o helper 

test: $(ALLOC_OBJS)
	$(CC) $(CFLAGS) $(ALLOC_OBJS) test.c -o test

%: %.c %.h
	$(CC) $(CFLAGS) -c $<


.PHONY: test clean

clean:
	rm -rf *.o
	rm -rf $(TEST_OBJS)
	rm -rf $(UTILS_OBJS)

